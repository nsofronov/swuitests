# Теперь напишем тест с использованем бд.

Для начала нужно настроить в my.properties 

* argus.db.address
* argus.db.port
* argus.db.name
* argus.db.user
* argus.db.password

Если всё верно, можно продолжить.

Открой IncidentsTest и посмотри на метод openIncidentsTest, отдельно от остальных тестов его можно запустить нажав на треугольник слева от метода.

```java
@Test
public void openIncidentsTest() {
    // Запрос, который ищет 5 последних инцидентов
    sqlQuery("SELECT BI_ID, TT_NUMBER FROM (SELECT * FROM ARGUS_SYS.BUSINESS_INTERACTION WHERE ENTITY_ID = 10067 ORDER BY OPEN_DATE DESC) WHERE ROWNUM <= 5").forEach(i -> {
        String bi_id = i.get("BI_ID");									// Id инцидента
        String tt_number = i.get("TT_NUMBER");							// Номер инцидента
        IncidentView incidentView = new IncidentView(bi_id);			// Открыть страницу инцидента
        incidentView.getHeader().shouldHave(Condition.text(tt_number));	// Проверить совпадение номера
    });
}
```

Начинается всё с 

```java
sqlQuery("SELECT BI_ID, TT_NUMBER FROM (SELECT * FROM ARGUS_SYS.BUSINESS_INTERACTION WHERE ENTITY_ID = 10067 ORDER BY OPEN_DATE DESC) WHERE ROWNUM <= 5")
```

Это выполнение запроса к базе. Этот запрос вернёт 5 последних созданных инцидентов причём только столбцы BI_ID, TT_NUMBER, другие нам не нужны.

Этот запрос можно выполнить отдельно и посмотреть, какие результаты он возвращает.

Потом идёт вызов.

```java
.forEach(i -> {
    // Какой-то текст
}
```

Это открытие цикла по результатам запроса. В этом цикле мы пройдём по всем полученным результатам и на каждый из них что-то сделаем.

```java
String bi_id = i.get("BI_ID");
String tt_number = i.get("TT_NUMBER");
```

Это получение результата из конкретной строчки в результате запроса, раз мы запрашивали BI_ID, TT_NUMBER, то и получать будем только их.

```java
IncidentView incidentView = new IncidentView(bi_id);
```

Это открытие страницы инцидента, если мы создаём new IncidentView(bi_id), то в браузере случится переход на конкретную страницу инцидента с переданным id

```java
incidentView.getHeader().shouldHave(Condition.text(tt_number));
```

Ну и остаётся только проверить, что открылся правильный инцидент и что это не страница ошибки, для этого поищем номер инцидента.
Номер находится в хедере страницы, поэтому там и будем искать (getHeader), а shouldHave(Condition.text()) нам в этом поможет.

Аналогично предлагаю проверить открытие последних групповых повреждений, можно добавить новый метод в GroupProblemTest. ENTITY_ID = 31394

Такой тест уже есть, но его можно написать повторно, например создав класс GroupProblemTest1 рядом с GroupProblemTest и постараться не смотря в исходный вавиан сделать свой.

## [А теперь ещё есть тесты для маленького приложения.](./fourth.md)
[На главную](../README.MD)
