# Как всё настроить и запустить первый тест за 5 минут.

### 1. [Скачать idea](https://www.jetbrains.com/idea/download) - Community Edition должно хватить
### 2. Установить, запустить и выбрать Check out from Version Control -> Git
### 3. Вставить "https://nsofronov@bitbucket.org/nsofronov/swuitests.git",  выбрать папку в которую проект скачается, нажать "Да"
### 4. В открывшемся проекте выбрать ClientSearchTest и запустить.

![1](pictures/quickstart1.png)

![2](pictures/quickstart2.png)

![3](pictures/quickstart3.png)

## Всё должно работать, но это не точно.

Что может пойти не так:

* [У idea должен быть свой git, если это не так, его придётся поставить](https://git-scm.com/downloads)
* [У idea должен быть свой maven, если это не так, то нужно будет его тоже поставить](https://maven.apache.org/download.cgi)
* Если idea предложит включить авто-импорт, его лучше включить.

Для начала этого должно хватить.

## [Далее - Как изучить java и стать разработчиком за 5 минут.](./becomedeveloper.md)

[На главную](../README.MD)