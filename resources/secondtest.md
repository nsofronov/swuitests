# Как написать свой второй тест, уже для аппсервера, ну, понятно за сколько.

### Для начала

Нужно запустить у себя аппсервер версии wip-3.20 и проверь содержимое файла my.properties (найти его можно по **ctrl + shift + N**).
В этом файле хранятся настройки для выполнения ui-тестов и главное **argus.server.address=http://localhost:8080/argus/** - адрес по которому будут эти тесты ходить.

Далее определяемся, что хочем проверить, например, наличие кнопок "Cегодня", "Вчера" и "Выбрать период" в истории действий на главной странице.

Для этого:

1. Логинимся
2. Открывается главная страница
3. Открываем "История действий"
4. Проверяем, что кнопки на месте

Начнём: С тестами Аргуса всё проще, поскольку уже есть небольшое api, чтобы упростить себе жизнь

### Открой тест MainPageTest

У него уже есть методы:

```java
@BeforeClass
public static void beforeClass() {
    prepare();
    login();
}
```

prepare() - создаст драйвер, выберет адрес по которому нужно обращаться и т.д. (Посмотреть что делает любой метод можно зажав **ctrl** и нажав на него левой кнопкой мыши)

login() - залогинится в системе.

```java
@AfterClass
public static void afterClass() {
    logout();
}
```

logout() - выход из системы, чтобы следующие тесты могли начинать с чистого листа.

```java
@Test
public void testRoles() {
    MainView mainPage = new MainView();
    mainPage.selectAboutSystemTab(2);
    SelenideElement worksitesAndRoles = $("#about_system .ui-tabs-panels > div", 2);
    worksitesAndRoles.$(".columns .column", 1).$("h3")
            .shouldBe(Condition.text("Личные роли"));
}
```

MainPage mainPage = new MainPage(); - создаёт экземпляр MainPage.
MainPage - это класс для удобства работы с главной страницей.
Если есть какое-то поведение (нажатие на кнопки, переключение вкладок, ввод текста в конкретное поле)
лучше выносить такое поведение в отдельные методы классов, которые не являются тест-кейсами,
чтобы эти методы могли вызывать другие тесты и тест кейсы, код не дублировался и был понятным
Открой его по "ctrl + left mouse", почитай java doc

mainPage.selectAboutSystemTab(2); - горовит переключить вкладку в центе страницы в позицию 3 ("Участки и роли") (нумерация начинается с 0)

```java
SelenideElement worksitesAndRoles = $("#about_system .ui-tabs-panels > div", 2);
```

worksitesAndRoles - содержимое вкладки "Уастки и роли"

```java
worksitesAndRoles.$(".columns .column", 1).$("h3")
        .shouldBe(Condition.text("Личные роли"));
```

- находит надпись Личные роли.

По аналогии с этим найдём кнопку Сегодня. Нужно создать новый метод и в нём:
переключиться на 4 вкладку (при нумерации с 0 на 3), и проверить наличие кнопки.

```java
MainView mainPage = new MainView();
mainPage.selectAboutSystemTab(3);
SelenideElement history = $("#about_system .ui-tabs-panels > div", 3);
history.$(".calc-container .ui-grid-row .ui-button", 0)
        .shouldBe(Condition.text("Сегодня"));
```

С нахождением остальных уже не должно быть проблем. (нужно просто поменять 0)


## [Далее - Осталось только написать тест с использованием вызовов к бд.](./thirdtest.md)
[На главную](../README.MD)