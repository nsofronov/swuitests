package ru.nsofronov.simpleweb;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@ViewScoped
@ManagedBean(name = "mainBean")
public class MainBean {
    private List<Integer> integers = new ArrayList<>();

    public List<Integer> getIntegers() {
        return integers;
    }

    public void addPanels() {
        integers.add(integers.stream().mapToInt(s -> s).max().orElse(0) + 1);
    }
}
