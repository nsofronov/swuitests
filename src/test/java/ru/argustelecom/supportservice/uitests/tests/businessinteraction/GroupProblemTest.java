package ru.argustelecom.supportservice.uitests.tests.businessinteraction;

import com.codeborne.selenide.Condition;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.argustelecom.supportservice.uitests.pages.businessinteraction.GroupProblemView;
import ru.argustelecom.supportservice.uitests.pages.businessinteraction.IncidentView;

import static ru.argustelecom.supportservice.uitests.DatabaseService.sqlQuery;
import static ru.argustelecom.supportservice.uitests.SWUiTestUtilites.prepare;
import static ru.argustelecom.supportservice.uitests.cases.businessinteraction.GroupProblemCase.closeGroupProblem;
import static ru.argustelecom.supportservice.uitests.cases.businessinteraction.GroupProblemCase.registerGroupProblem;
import static ru.argustelecom.supportservice.uitests.pages.AbstractArgusView.logout;
import static ru.argustelecom.supportservice.uitests.pages.LoginView.login;

/**
 * Набор тестов для проверки групповых повреждений
 *
 * @author n.sofronov
 */
public class GroupProblemTest {
	@BeforeClass
	public static void beforeClass() {
		prepare();
		login();
	}

	/**
	 * Тест открываемости последних созданных ГП
	 */
	@Test
	public void openGroupProblemsTest() {
		// Запрос, который ищет 5 последних ГП
		sqlQuery("SELECT BI_ID, TT_NUMBER FROM (SELECT * FROM ARGUS_SYS.BUSINESS_INTERACTION WHERE ENTITY_ID = 31394 ORDER BY OPEN_DATE DESC) WHERE ROWNUM <= 5").forEach(i -> {
			String bi_id = i.get("BI_ID");										// Id ГП
			String tt_number = i.get("TT_NUMBER");								// Номер ГП
			GroupProblemView groupProblemView = new GroupProblemView(bi_id);	// Открыть страницу ГП
			groupProblemView.getHeader().shouldHave(Condition.text(tt_number));	// Проверить совпадение номера
		});
	}

	/**
	 * Тест создания группового повреждения
	 */
	@Test
	public void registerIncidentUnknownServiceTest(){
		GroupProblemView groupProblemView = registerGroupProblem();	// Регистрация ГП
		closeGroupProblem(groupProblemView);						// Закрытие ГП
	}

	@AfterClass
	public static void afterClass() {
		logout();
	}
}
