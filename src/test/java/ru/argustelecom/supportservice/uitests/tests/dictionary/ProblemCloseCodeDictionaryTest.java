package ru.argustelecom.supportservice.uitests.tests.dictionary;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.argustelecom.supportservice.uitests.pages.dictionary.ProblemCloseCodeDirView;
import ru.argustelecom.supportservice.uitests.pages.main.MainView;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static ru.argustelecom.supportservice.uitests.DatabaseService.sqlQuery;
import static ru.argustelecom.supportservice.uitests.DatabaseService.sqlUpdate;
import static ru.argustelecom.supportservice.uitests.SWUiTestUtilites.prepare;
import static ru.argustelecom.supportservice.uitests.SWUiTestUtilites.randomName;
import static ru.argustelecom.supportservice.uitests.cases.dictionaries.ProblemCloseCodeDictionaryCase.createCloseCode;
import static ru.argustelecom.supportservice.uitests.cases.dictionaries.ProblemCloseCodeDictionaryCase.createCodeCategory;
import static ru.argustelecom.supportservice.uitests.cases.dictionaries.ProblemCloseCodeDictionaryCase.deactivateCode;
import static ru.argustelecom.supportservice.uitests.cases.dictionaries.ProblemCloseCodeDictionaryCase.editCloseCode;
import static ru.argustelecom.supportservice.uitests.cases.dictionaries.ProblemCloseCodeDictionaryCase.editCodeCategory;
import static ru.argustelecom.supportservice.uitests.cases.dictionaries.ProblemCloseCodeDictionaryCase.removeCodeCategory;
import static ru.argustelecom.supportservice.uitests.pages.AbstractArgusView.logout;
import static ru.argustelecom.supportservice.uitests.pages.LoginView.login;

/**
 * @author n.sofronov
 */
public class ProblemCloseCodeDictionaryTest {
	private static ProblemCloseCodeDirView view;

	@BeforeClass
	public static void beforeClass() {
		prepare();
		login();
		view = new MainView().openProblemCloseCodeDirView();
	}

	/**
	 * Тест создания категории ОЧЗ
	 */
	@Test
	public void dictionaryTest() {
		// Создаём категорию ШЗ и проверяем, что она создалась в дереве
		String parentCategory = createCodeCategory(view, null, randomName("UI"));
		assertNotNull(view.findCodeCategory(null, parentCategory));

		// Создаём подкатегорию ШЗ и проверяем, что она создалась в дереве
		String childCategory = createCodeCategory(view, parentCategory, randomName("UI"));
		assertNotNull(view.findCodeCategory(parentCategory, childCategory));

		// Поредактируем категорию ШЗ
		childCategory = editCodeCategory(view, childCategory, randomName("UI"));

		// Создаём шифр закрытия
		String codeName = createCloseCode(view, randomName("UI-шифр"), childCategory, randomName("UI-описание"));

		// Отредактируем шифр закрытия
		codeName = editCloseCode(view, codeName, randomName("UI-шифр"));

		// Деактивируем шифр закрытия и проверим, что он корректно поменял статус в базе
		deactivateCode(view, codeName);
		List<Map<String, String>> result = sqlQuery("SELECT OBJECT_STATUS_ID FROM CLOSE_CODE WHERE CLOSE_CODE_NAME  = '" + codeName + "'");
		assertEquals(1, result.size());
		assertEquals(257 + "", result.get(0).get("OBJECT_STATUS_ID"));

		// Удаляем категории
		removeCodeCategory(view, parentCategory);

		// Удаляем шифр из базы
		sqlUpdate("DELETE FROM CLOSE_CODE WHERE CLOSE_CODE_NAME = '" + codeName + "'");
	}

	@AfterClass
	public static void afterClass() {
		logout();
	}
}
