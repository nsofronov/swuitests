package ru.argustelecom.supportservice.uitests.tests.search;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import ru.argustelecom.supportservice.uitests.pages.LoginView;
import ru.argustelecom.supportservice.uitests.pages.search.ServiceSearchPage;

/**
 * Created by el.vasilyeva on 06.06.2017.
 */
public class ServiceSearchTest {

    @BeforeClass
    public static void BeforeClass() {
        System.setProperty("webdriver.chrome.driver", ".\\src\\test\\resources\\driver\\chromedriver_v2.29.exe");
        Configuration.baseUrl = "http://192.168.101.15:8080/argus/";
        //setWebDriver(new ChromeDriver());
        Configuration.browser = "chrome";
        new LoginView().setLoginName("w")
                .setPassword("w")
                .click(); //логинит
    }

    @Before
    public void Before() {
//        $(By.linkText("Обращения")).hover(); //наводит курсор на "Обращения"
//        $(By.linkText("Поиск услуги (ALT+1)")).waitUntil(Condition.visible, 5000).click(); //выбирает "Поиск услуги"
    }

    @Test
    public void SearchByRegionAdressAndPartialServiceName() {
        ServiceSearchPage serviceSearchPage = new ServiceSearchPage();
        serviceSearchPage.clickSearviceSearch(); //переходит на поиск услуги
        serviceSearchPage.setServiceName("868"); //вводит частичное имя услуги
        serviceSearchPage.selectOrg(); //выбирает ЮЛ
        serviceSearchPage.setServiceAdress("Санкт"); //вводит Санкт
        $(By.xpath(".//*[@id='search_params_form-address-address_installation_panel']/ul/li[2]")).click(); //выбирается Санкт-Петербург (i_i)
        serviceSearchPage.clickSearchButton(); //нажимает кнопку поиска
        $(By.xpath(".//*[@id='search_result_form-j_idt238-0-j_idt253-0-j_idt255_data']/tr[1]/td[1]/a")).shouldHave(Condition.text("868"));
        //проверяет, что в имени первой найденной услуги содержатся искомые символы
    }

    @Test
    public void SearchByBuildingAdressAndServiceType() {
        ServiceSearchPage serviceSearchPage = new ServiceSearchPage();
        serviceSearchPage.clickSearviceSearch(); //переходит на поиск услуги
        $(By.xpath(".//*[@id='search_dialog_form']/p[2]")).waitUntil(Condition.disappear, 5000); //ждет
        serviceSearchPage.setServiceAdress("смольный 1 "); //вводит частичный адрес
        $(By.xpath(".//*[@id='search_params_form-address-address_installation_panel']/ul/li")).click(); //кличет по совпадению
        serviceSearchPage.selectClientType("ЮЛ");  //выбирает ЮЛ
        serviceSearchPage.setServiceType("Прямая"); //вводит тип услуги, кличет на более подходящем типе
        serviceSearchPage.clickSearchButton(); //нажимает кнопку поиска
        $(By.xpath(".//*[@id='search_result_form-j_idt238-0-j_idt253-0-j_idt255_data']/tr[1]/td[2]")).shouldHave(Condition.text("Прямая"));
        //проверяет, что тип услуги искомый
        $(By.xpath(".//*[@id='search_result_form-j_idt238-0-j_idt253-0-j_idt254']/legend")).shouldHave(Condition.text("РФ, г. Санкт-Петербург (i_i), проезд Смольный, 1"));
        //проверят, что адрес искомый

    }

    @Test
    public void SearchByRegionAdressAndFullServiceName() {
        ServiceSearchPage serviceSearchPage = new ServiceSearchPage();
        serviceSearchPage.clickSearviceSearch(); //переходит на поиск услуги
        serviceSearchPage.setServiceName("8123121669"); //вводит полное имя услуги
        serviceSearchPage.setServiceAdress("Санкт"); //вводит Санкт
        $(By.xpath(".//*[@id='search_params_form-address-address_installation_panel']/ul/li[2]")).click(); //выбирается Санкт-Петербург (i_i)
        serviceSearchPage.selectClientType("ЮЛ"); //выбирает ЮЛ
        serviceSearchPage.clickSearchButton(); //нажимает кнопку поиска
        $(By.xpath(".//*[@id='client_tabs-clientInstallationsForm-client_installations_table_data']/tr/td[2]")).shouldHave(Condition.text("(812)3121669"));
        //сравнивает имя услуги с введенным при поиске

    }
    @Test
    public void SearchByBuildingAdressAndClientType() {
        ServiceSearchPage serviceSearchPage = new ServiceSearchPage();
        serviceSearchPage.clickSearviceSearch(); //переходит на поиск услуги
        $(By.xpath(".//*[@id='search_dialog_form']/p[2]")).waitUntil(Condition.disappear, 5000); //ждет
        serviceSearchPage.setServiceAdress("смольный 1 "); //вводит частичный адрес
        $(By.xpath(".//*[@id='search_params_form-address-address_installation_panel']/ul/li")).click(); //кличет по совпадению
        serviceSearchPage.selectOrg(); //выбирает ЮЛ
        serviceSearchPage.clickSearchButton(); //нажимает кнопку поиска
        $(By.xpath(".//*[@id='search_ui_grid']/div[1]/div[2]")).shouldHave(Condition.text("ЮЛ")); //проверяет, что ЮЛ
        $(By.xpath(".//*[@id='search_result_form-j_idt238-0-j_idt253-0-j_idt254']/legend")).shouldHave(Condition.text("РФ, г. Санкт-Петербург (i_i), проезд Смольный, 1"));
        //проверяет, что адрес совпадает с введенным при поиске

    }

    @Test
    public void SearchByNLS() {
        ServiceSearchPage serviceSearchPage = new ServiceSearchPage();
        serviceSearchPage.clickSearviceSearch(); //переходит на поиск услуги
        serviceSearchPage.setServiceAdress("Архан"); //вводит Архан
        $(By.xpath(".//*[@id='search_params_form-address-address_installation_panel']/ul/li[2]")).shouldBe(visible);
        $(By.xpath(".//*[@id='search_params_form-address-address_installation_panel']/ul/li[2]")).click(); //выбирается Архангельскую обл
        serviceSearchPage.selectNLS(); //выбирает НЛС
        serviceSearchPage.setClientDetail("193907673"); //вводит НЛС
        serviceSearchPage.selectClientType("ФЛ"); //выбирает ФЛ
        serviceSearchPage.clickSearchButton(); //нажимает кнопку поиска
        $(By.xpath(".//*[@id='search_ui_grid']/div[1]/div[2]")).shouldBe(Condition.appears);
        $(By.xpath(".//*[@id='search_ui_grid']/div[1]/div[2]")).shouldHave(Condition.text("193907673")); //проверяет, что НЛС клиента совпадает с введенным при поиске

    }

    @Test
    public void SearchByINN() {
        ServiceSearchPage serviceSearchPage = new ServiceSearchPage();
        serviceSearchPage.clickSearviceSearch(); //переходит на поиск услуги
        serviceSearchPage.setServiceAdress("Санкт"); //вводит Санкт
        $(By.xpath(".//*[@id='search_params_form-address-address_installation_panel']/ul/li[2]")).shouldBe(visible);
        $(By.xpath(".//*[@id='search_params_form-address-address_installation_panel']/ul/li[2]")).click(); //выбирается Санкт-Петербург (i_i)
        serviceSearchPage.selectINN(); //выбирает ИНН
        serviceSearchPage.setClientDetail("7826049429"); //вводит ИНН
        serviceSearchPage.selectClientType("ЮЛ"); //выбирает ЮЛ
        serviceSearchPage.clickSearchButton(); //нажимает кнопку поиска
        $(By.xpath(".//*[@id='search_ui_grid']/div/div[1]/a")).shouldBe(Condition.appears);
        $(By.xpath(".//*[@id='search_ui_grid']/div/div[1]/a")).click(); //переходит в КК первого найденного клиента
        $(By.xpath(".//*[@class='ui-grid-col-7_5 face']//*[last()]")).shouldHave(Condition.text("7826049429")); //проверяет, что ИНН клиента совпадает с введенным при поиске

    }

    @Test
    public void SearchForAllowDeactivated() {
        ServiceSearchPage serviceSearchPage = new ServiceSearchPage();
        serviceSearchPage.clickSearviceSearch(); //переходит на поиск услуги
        serviceSearchPage.setServiceName("%%%"); //вводит имя услуги
        serviceSearchPage.setAllowDeactivated(false); //снимает выбор с флага "Только активированные"
        $(By.xpath(".//*[@id='search_dialog_form']/p[2]")).waitUntil(Condition.disappear, 50000); //ждет
        serviceSearchPage.clickSearchButton(); //нажимает кнопку поиска
        //serviceSearchPage.getSearchResult().shouldHave();


    }

    @Test
    public void SearchAndClean() {
        ServiceSearchPage serviceSearchPage = new ServiceSearchPage();
        serviceSearchPage.setServiceName("8123121669"); //вводит имя услуги
        serviceSearchPage.setServiceAdress("Санкт"); //вводит Санкт
        $(By.xpath(".//*[@id='search_params_form-address-address_installation_panel']/ul/li[2]")).click(); //выбирается Санкт-Петербург (i_i)
        serviceSearchPage.setServiceType("Прямая"); //вводит тип услуги
        serviceSearchPage.setTechnologyType("ADSL"); //вводит технологию
        serviceSearchPage.setClientDetail("государственное"); //вводит имя клиента
        serviceSearchPage.clickClean(); //нажимает кнопку очистки
        $(By.id("search_params_form-service_name")).shouldHave(Condition.text("")); //проверяет, что Имя услуги очищено
        $(By.id("search_params_form-address-address_installation_input")).shouldHave(Condition.text("")); //проверяет, что Адрес очищено
        $(By.id("search_params_form-service_type_input")).shouldHave(Condition.text("")); //проверяет, что Тип услуги очищено
        $(By.id("search_params_form-inst_load_type_input")).shouldHave(Condition.text("")); //проверяет, что Технология очищено
        $(By.id("search_params_form-client_detail")).shouldHave(Condition.text("")); //проверяет, что Имя клиента очищено

    }





}