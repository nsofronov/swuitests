package ru.argustelecom.supportservice.uitests.tests.businessinteraction;

import com.codeborne.selenide.Condition;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.argustelecom.supportservice.uitests.pages.businessinteraction.IncidentView;

import java.io.Serializable;

import static ru.argustelecom.supportservice.uitests.DatabaseService.sqlQuery;
import static ru.argustelecom.supportservice.uitests.SWUiTestUtilites.prepare;
import static ru.argustelecom.supportservice.uitests.cases.businessinteraction.IncidentCase.closeIncident;
import static ru.argustelecom.supportservice.uitests.cases.businessinteraction.IncidentCase.registerIncidentUnknownService;
import static ru.argustelecom.supportservice.uitests.pages.AbstractArgusView.logout;
import static ru.argustelecom.supportservice.uitests.pages.LoginView.login;

/**
 * Набор тестов для проверки инцидентов
 *
 * @author n.sofronov
 */
public class IncidentsTest implements Serializable {
	@BeforeClass
	public static void beforeClass() {
		prepare();
		login();
	}

	/**
	 * Тест открываемости последних созданных инцидентов
	 */
	@Test
	public void openIncidentsTest() {
		// Запрос, который ищет 5 последних инцидентов
		sqlQuery("SELECT BI_ID, TT_NUMBER FROM (SELECT * FROM ARGUS_SYS.BUSINESS_INTERACTION WHERE ENTITY_ID = 10067 ORDER BY OPEN_DATE DESC) WHERE ROWNUM <= 5").forEach(i -> {
			String bi_id = i.get("BI_ID");									// Id инцидента
			String tt_number = i.get("TT_NUMBER");							// Номер инцидента
			IncidentView incidentView = new IncidentView(bi_id);			// Открыть страницу инцидента
			incidentView.getHeader().shouldHave(Condition.text(tt_number));	// Проверить совпадение номера
		});
	}

	/**
	 * Тест регистрации инцидента на неизвестную услугу
	 */
	@Test
	public void registerIncidentUnknownServiceTest(){
		IncidentView incidentView = registerIncidentUnknownService(400026159L);	// Регистрация инцидента
		closeIncident(incidentView);													// Закрытие инцидента
	}

	@AfterClass
	public static void afterClass() {
		logout();
	}
}
