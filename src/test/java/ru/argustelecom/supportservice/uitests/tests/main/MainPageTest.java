package ru.argustelecom.supportservice.uitests.tests.main;

import static com.codeborne.selenide.Selenide.$;
import static ru.argustelecom.supportservice.uitests.SWUiTestUtilites.prepare;
import static ru.argustelecom.supportservice.uitests.pages.AbstractArgusView.logout;
import static ru.argustelecom.supportservice.uitests.pages.LoginView.login;

import com.codeborne.selenide.SelenideElement;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.codeborne.selenide.Condition;

import ru.argustelecom.supportservice.uitests.pages.main.MainView;

/**
 * Тест главной страницы, если она не открывается, значит сервер не работает
 *
 * @author n.sofronov
 */
public class MainPageTest  {

	@BeforeClass
	public static void beforeClass() {
		prepare();
		login();
	}

	@Test
	public void testRoles() {
		MainView mainPage = new MainView();
		mainPage.selectAboutSystemTab(2);
		SelenideElement worksitesAndRoles = $("#about_system .ui-tabs-panels > div", 2);
		worksitesAndRoles.$(".columns .column", 1).$("h3")
				.shouldBe(Condition.text("Личные роли"));
	}

	@AfterClass
	public static void afterClass() {
		logout();
	}
}
