package ru.argustelecom.supportservice.uitests.tests.search;

import static com.codeborne.selenide.Selenide.$;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import ru.argustelecom.supportservice.uitests.pages.LoginView;
import ru.argustelecom.supportservice.uitests.pages.search.ClientSearchPage;

/**
 * Created by el.vasilyeva on 05.06.2017.
 */
public class ClientSearchTest {

    @BeforeClass
    public static void BeforeClass() {
        System.setProperty("webdriver.chrome.driver", ".\\src\\test\\resources\\driver\\chromedriver_v2.29.exe");
        Configuration.baseUrl = "http://192.168.101.15:8080/argus/";
        //setWebDriver(new ChromeDriver());
        Configuration.browser = "chrome";
            new LoginView().setLoginName("w")
                .setPassword("w")
                .click(); //логинит
    }

    @Before
    public void Before(){
    //    $(By.linkText("Обращения")).hover(); //наводит курсор на "Обращения"
    //    $(By.linkText("Поиск клиента (ALT+2)")).waitUntil(Condition.visible, 5000).click(); //выбирает "Поиск клиента"
    }

    @Test
    public void SearchByContactNumber(){
        ClientSearchPage clientSearchPage = new ClientSearchPage(); // Selenide.open("views/registercontact/search/clients/SearchView.xhtml", Search.ClientSearchPage.class); //открывается страница поиска клиента
        clientSearchPage.clickClientSearch(); //переходит на поиск клиента
        // clientSearchPage.selectAll(); //тип клиента - Все

        clientSearchPage.setContactPhone("1111111111"); //контактный номер
        clientSearchPage.clickSearchButton(); //нажимает кнопку поиска
        $(By.xpath(".//*[@id='search_ui_grid']/div[1]/div[1]/a")).click(); //кличет первую ссылку найденных клиентов
        $(By.xpath(".//*[@id='client_tabs']/ul/li[3]/a")).click(); //переходит в контакты
        $(By.xpath(".//*[@id='client_tabs-contact_info_tab_content-contact_info_accordion-main_contacts_info_table_data']/tr/td[3]")).shouldBe(Condition.text("1111111111"));

    }

    @Test
    public void SearchByINN() {
        ClientSearchPage clientSearchPage = new ClientSearchPage();
        clientSearchPage.clickClientSearch(); //переходит на поиск клиента
        clientSearchPage.selectOrg(); //тип клиента - ЮЛ
        clientSearchPage.selectINN(); //выбирает ИНН
        clientSearchPage.setClientDetail("7827001363"); //ИНН
        clientSearchPage.setAdress("Санкт"); //частичный ввод региона
        $(By.xpath(".//*[@id='search_params_form-address-address_client_panel']/ul/li[2]")).click(); //выбирается Санкт-Петербург (i_i)
        clientSearchPage.clickSearchButton(); //нажимает кнопку поиска
        //String openedClientInn = clientSearchPage.getClientINN();
        $(By.xpath(".//*[@class='ui-grid-col-7_5 face']//*[last()]")).shouldHave(Condition.text("7827001363")); //сравнивает ИНН клиента с искомым

    }

    @Test
    public void SearchByName() {
        ClientSearchPage clientSearchPage = new ClientSearchPage();
        clientSearchPage.clickClientSearch(); //переходит на поиск клиента
        clientSearchPage.selectOrg(); //тип клиента ЮЛ
        clientSearchPage.selectName(); //кнопка "Имя"
        clientSearchPage.setClientDetail("государственное"); //ввод частичного имени  клиента
        clientSearchPage.setAdress("Санкт"); //частичный ввод региона
        $(By.xpath(".//*[@id='search_params_form-address-address_client_panel']/ul/li[2]")).click(); //выбирается Санкт-Петербург (i_i)
        clientSearchPage.clickSearchButton(); //нажимает кнопку поиска
        $(By.xpath(".//*[@id='search_ui_grid']/div[1]/div[1]/a")).click(); //кличет первую ссылку найденных клиентов
        $(By.xpath(".//*[@id='client_tabs-clientInstallationsForm-client_installations_table_data']/tr[1]/td[1]")).click(); //выбирает первую услугу в КК
        $(By.xpath(".//*[@id='installation_edit_form']/div/dl/dd[1]")).shouldHave(Condition.text("государственное")); //проверяет содержание искомого слова в имени клиента
    }

    @Test
    public void SearchByNLS(){
        ClientSearchPage clientSearchPage = new ClientSearchPage();
        clientSearchPage.clickClientSearch(); //переходит на поиск клиента
        clientSearchPage.selectAbon(); //тип клиента ФЛ
        clientSearchPage.setAdress("Санкт"); //частичный ввод региона
        $(By.xpath(".//*[@id='search_params_form-address-address_client_panel']/ul/li[2]")).click(); //выбирается Санкт-Петербург (i_i)
        clientSearchPage.selectNLS(); //выбирает НЛС
        clientSearchPage.setClientDetail("-388957159"); //заполняет НЛС
        clientSearchPage.clickSearchButton(); //нажимает кнопку поиска
        $(By.xpath(".//*[@class='ui-grid-col-7_5 face']//*[last()]")).shouldHave(Condition.text("-388957159")); //сравнивает НЛС клиента с введенным при поиске НЛС

}

    @Test
    public void SearchAndClean() {
        ClientSearchPage clientSearchPage = new ClientSearchPage();
        clientSearchPage.clickClientSearch(); //переходит на поиск клиента
        clientSearchPage.selectOrg(); //тип клиента - ЮЛ
        clientSearchPage.selectName(); //выбирает Имя
        clientSearchPage.setClientDetail("государственное"); //ИНН
        clientSearchPage.setContactPhone("1111111111"); //контактный номер
        clientSearchPage.setAdress("Санкт"); //частичный ввод региона
        $(By.xpath(".//*[@id='search_params_form-address-address_client_panel']/ul/li[2]")).click(); //выбирается Санкт-Петербург (i_i)
        clientSearchPage.clickClean(); //нажимает кнопку очистки
        $(By.id("search_params_form-client_detail")).shouldHave(Condition.text("")); //проверяет, что Имя очищено
        $(By.id("search_params_form-phone")).shouldHave(Condition.text("")); //проверяет, что Контактный телефон очищено
        $(By.id("search_params_form-address-address_client")).shouldHave(Condition.text("")); //проверяет, что Адрес очищено
        $(By.id("search_params_form-location")).shouldHave(Condition.text("")); //проверяет, что Кв. очищено

    }
}
