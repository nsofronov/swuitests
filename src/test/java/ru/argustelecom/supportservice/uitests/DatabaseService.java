package ru.argustelecom.supportservice.uitests;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.argustelecom.supportservice.uitests.PropertiesService.DB_ADDRESS;
import static ru.argustelecom.supportservice.uitests.PropertiesService.DB_NAME;
import static ru.argustelecom.supportservice.uitests.PropertiesService.DB_PASSWORD;
import static ru.argustelecom.supportservice.uitests.PropertiesService.DB_PORT;
import static ru.argustelecom.supportservice.uitests.PropertiesService.DB_USER;
import static ru.argustelecom.supportservice.uitests.PropertiesService.getProperty;

/**
 * Сервис для работы с базой данных
 *
 *@Test
 *public void testQuery() {
 *	sqlQuery("select CODE_ID, CODE_DESCRIPTION from argus_sys.CODE").forEach(m -> {
 *		m.forEach((key, value) -> System.out.print(key+": " + value + "; "));
 *		System.out.println("");
 *	});
 *}
 *
 * @author n.sofronov
 */
public class DatabaseService implements Serializable {
	private static DatabaseService instance;
	private Connection connection;

	private static DatabaseService getInstance() {
		if(instance == null) {
			instance = new DatabaseService();
		}
		return instance;
	}

	private DatabaseService() {
		startConnection();
	}

	private void startConnection() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			String url = "jdbc:oracle:thin:@" + getProperty(DB_ADDRESS) + ":" + getProperty(DB_PORT) + ":" + getProperty(DB_NAME) + "";
			connection = DriverManager.getConnection(url, getProperty(DB_USER), getProperty(DB_PASSWORD));
			if (connection != null) {
				System.out.println("Создано подключение к базе " + url);
			} else {
				System.out.println("Ошибка создания соединения " + url);
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Выполяет sql запрос к базе
	 *
	 * @param sql текст sql запроса
	 * @return результат выполнения запроса
	 */
	public static List<Map<String, String>> sqlQuery(String sql){
		System.out.println("Выполняю запрос: " + sql);
		Statement statement = null;
		try {
			List<Map<String, String>> result = new ArrayList<>();
			Connection connection = getInstance().connection;
			statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			ResultSetMetaData metaData = rs.getMetaData();
			while (rs.next()) {
				Map<String, String> map = new HashMap<>();
				for (int i = 1; i<=metaData.getColumnCount(); i++){
					String columnName = metaData.getColumnName(i);
					map.put(columnName, rs.getString(columnName));
				}
				result.add(map);
			}
			System.out.println("Запрос вернул " + result.size() + " записей");
			return result;
		} catch (Throwable e) {
			e.printStackTrace();
			return Collections.emptyList();
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Запрос на обновление
	 *
	 * @param sql запрос
	 * @return количество обновлённых записей
	 */
	public static int sqlUpdate(String sql){
		System.out.println("Выполняю запрос на обновление: " + sql);
		Statement statement = null;
		try {
			List<Map<String, String>> result = new ArrayList<>();
			Connection connection = getInstance().connection;
			statement = connection.createStatement();
			int update = statement.executeUpdate(sql);
			System.out.println("Запрос обновил " + update + " записей");
			return update;
		} catch (Throwable e) {
			e.printStackTrace();
			return 0;
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Завершение подключения к базе
	 */
	public static void finishConnection() {
		if (instance != null) {
			try {
				instance.connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			instance = null;
		}
	}
}
