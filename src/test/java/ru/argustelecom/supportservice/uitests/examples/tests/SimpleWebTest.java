package ru.argustelecom.supportservice.uitests.examples.tests;

import com.codeborne.selenide.Configuration;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.argustelecom.supportservice.uitests.examples.components.Button;
import ru.argustelecom.supportservice.uitests.examples.components.Label;
import ru.argustelecom.supportservice.uitests.examples.pages.SimpleWebPage;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SimpleWebTest {
    @BeforeClass
    public static void beforeClass() {
        System.setProperty("webdriver.chrome.driver", ".\\src\\test\\resources\\driver\\chromedriver_v2.29.exe");
        Configuration.baseUrl = "http://localhost:8080/simple-web-1.0/index.xhtml";
        Configuration.browser = "chrome";
    }

    @Test
    public void searchBaseLabel(){
        SimpleWebPage simpleWebPage = new SimpleWebPage();              // Открыть нужную страницу
        Label baseLabel = simpleWebPage.getBaseLabel();                 // Получить верхнюю надпись страницы
        assertNotNull(baseLabel);                                       // Проверить что эта надпись есть
        assertEquals("Верхняя надпись", baseLabel.getText());  // Проверить текст этой надписи
    }

    @Test
    public void openOnePanel() throws InterruptedException {
        // Открыть нужную страницу
        SimpleWebPage simpleWebPage = new SimpleWebPage();

        // Найти кнопку
        Button button = simpleWebPage.getPanelButton();

        // Проверить, что ещё нет записей
        List<Label> labels = simpleWebPage.getLabels();
        assertEquals(0, labels.size());

        // Нажать на кнопку и проверить, что записи появились
        button.click();
        labels = simpleWebPage.getLabels();
        assertEquals(1, labels.size());
    }
}
