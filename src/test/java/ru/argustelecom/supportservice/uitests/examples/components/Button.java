package ru.argustelecom.supportservice.uitests.examples.components;

import com.codeborne.selenide.SelenideElement;

/**
 * Кнопка в простом приложении
 */
public class Button {
    private SelenideElement element;

    public Button(SelenideElement element) {
        this.element = element;
    }

    public synchronized void click() {
        element.click();
        try {
            wait(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
