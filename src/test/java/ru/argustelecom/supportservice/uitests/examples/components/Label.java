package ru.argustelecom.supportservice.uitests.examples.components;

import com.codeborne.selenide.SelenideElement;

/**
 * Текст в простом приложении
 */
public class Label {
    private SelenideElement element;

    public Label(SelenideElement element) {
        this.element = element;
    }

    public String getText() {
        return element.getText();
    }
}
