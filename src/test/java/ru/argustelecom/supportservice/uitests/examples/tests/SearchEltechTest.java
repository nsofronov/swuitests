package ru.argustelecom.supportservice.uitests.examples.tests;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.Serializable;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

/**
 * Поиск по сайту "ЛЭТИ"
 *
 * @author n.sofronov
 */
public class SearchEltechTest implements Serializable {

	@BeforeClass
	public static void beforeClass() {
		System.setProperty("webdriver.chrome.driver", ".\\src\\test\\resources\\driver\\chromedriver_v2.29.exe");
		Configuration.baseUrl = "http://www.eltech.ru/";
		Configuration.browser = "chrome";
	}

	@Test
	public void searchManTest(){
		open("");

		String name = "Спиваковский Александр Михайлович";

		$(".form-control").setValue(name);	// Ввести имя в поле поиска
		$(".form-control").pressEnter();		// Нажать "Enter"

		$("#center .Workspace h2 a").click();	// Выбрать первый результат поиска

		$(".page-header").shouldBe(Condition.text(name));	// Проверить результат
	}
}
