package ru.argustelecom.supportservice.uitests.examples.pages;

import org.openqa.selenium.By;
import ru.argustelecom.supportservice.uitests.examples.components.Button;
import ru.argustelecom.supportservice.uitests.examples.components.Label;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;

/**
 * Страничка простого приложения
 */
public class SimpleWebPage {
    public SimpleWebPage() {
        open("");
    }

    public Label getBaseLabel() {
        return new Label($("#globalText"));
    }

    public Button getPanelButton() {
        return new Button($(By.id("baseForm:panelButton")));
    }

    public List<Label> getLabels() {
        return $$(".panel").stream().map(Label::new).collect(Collectors.toList());
    }
}
