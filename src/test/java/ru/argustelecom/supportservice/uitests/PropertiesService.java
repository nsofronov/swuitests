package ru.argustelecom.supportservice.uitests;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;

/**
 * Сервис для работы с настройками UI тестов
 *
 * @author n.sofronov
 */
public class PropertiesService implements Serializable {

	/**
	 * Адрес тестируемого аппсервера
	 */
	public static String SERVER_ADDRESS = "argus.server.address";

	/**
	 * Логин на тестируемом аппсервере
	 */
	public static String SERVER_LOGIN = "argus.server.login";

	/**
	 * Пароль на тестируемом аппсервере
	 */
	public static String SERVER_PASSWORD = "argus.server.password";

	/**
	 * Адреес базы данных для тестов
	 */
	public static String DB_ADDRESS = "argus.db.address";

	/**
	 * Порт базы данных для тестов
	 */
	public static String DB_PORT = "argus.db.port";

	/**
	 * SID базы данных для тестов
	 */
	public static String DB_NAME = "argus.db.name";

	/**
	 * Логин для подключения к базе
	 */
	public static String DB_USER = "argus.db.user";

	/**
	 * Пароль для подключения к базе
	 */
	public static String DB_PASSWORD = "argus.db.password";

	private static PropertiesService instance;
	private Properties prop;

	/**
	 * Возвращает настройку UI тестов по ключу
	 *
	 * @param key - ключ
	 * @return настрока
	 */
	public static String getProperty(String key) {
		return getInstance().prop.getProperty(key, "");
	}

	private static PropertiesService getInstance() {
		if(instance == null) {
			instance = new PropertiesService();
		}
		return instance;
	}

	private PropertiesService() {
		prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream(".\\src\\test\\resources\\my.properties");
			prop.load(input);
			prop.forEach((key, value) -> System.out.println(key + ": " +value ));
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
