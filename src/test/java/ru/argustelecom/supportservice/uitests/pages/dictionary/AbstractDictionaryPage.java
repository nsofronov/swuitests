package ru.argustelecom.supportservice.uitests.pages.dictionary;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import ru.argustelecom.supportservice.uitests.pages.AbstractArgusView;

import java.util.function.Function;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static ru.argustelecom.supportservice.uitests.SWUiTestUtilites.findElement;

/**
 * Абстрактная страница справочника
 *
 * @author n.sofronov
 */
public class AbstractDictionaryPage extends AbstractArgusView {
	public static final Function<SelenideElement, String> TITLE = b -> b.getAttribute("title");
	public static final Function<SelenideElement, String> VALUE = b -> b.getAttribute("value");

	/**
	 * Нажатие на кнопку создания новой записи в справочнике
	 * 
	 * @return диалог создания
	 */
	public SelenideElement clickAddButton() {
		getAddButton().click();			// Нажать на кнопку
		return getCreateItemDialog();	// Откроется диалог создания, его и возвращаем
	}

	/**
	 * Нажатие на кнопку деактивации верхней строки в таблице
	 */
	public void clickDeactivate() {
		getDeactivateButton().click();		// Нажать на кнопку
		dialogOkbp(getDeactivateDialog());	// Откроется диалог подтверждения, в нем подтверждаем действие
	}

	/**
	 * Нажатие на кнопку редактирования верхней строки в таблице
	 */
	public void clickEditRow() {
		getFirstRow().$(".ui-row-editor .ui-icon-pencil").waitUntil(Condition.visible, 5000).click();
	}

	/**
	 * Замена значения в одном из текстовых полей в таблице
	 *
	 * @param currentValue	- текущее значение
	 * @param newValue		- новое значение
	 */
	public void editRowField(String currentValue, String newValue) {
		findElement(getFirstRow().$$("input"), VALUE, currentValue).setValue(newValue);	// Замена значение в нужном поле в первой строке
	}

	/**
	 * Подтверждение редактирования строки
	 */
	public void saveEdit() {
		getFirstRow().$(".ui-row-editor .ui-icon-check").waitUntil(Condition.visible, 5000).click();	// Нажать на кнопку редактирования в первой стргоке таблицы
	}

	/**
	 * Возвращает хедер страницы справочника
	 *
	 * @return хедер страницы справочника
	 */
	private SelenideElement getHeader() {
		return $("#wrap_header");
	}

	/**
	 * Возвращает таблицу на странице справочника
	 *
	 * @return таблица на странице справочника
	 */
	protected SelenideElement getTableData() {
		return $("#edit_form-edit_table_data");
	}

	/**
	 * Возвращает кнопку создания новой записи в таблице справочника
	 *
	 * @return кнопка создания новой записи в таблице справочника
	 */
	private SelenideElement getAddButton() {
		return getHeader().$("button");
	}

	/**
	 * Возвращает диалог создания новой записи в таблице справочника
	 *
	 * @return диалог создания новой записи в таблице справочника
	 */
	private SelenideElement getCreateItemDialog() {
		return $("#create_item_dialog");
	}

	/**
	 * Возвращает кнопку деактивации из первой строки таблицы
	 *
	 * @return кнопка деактивации из первой строки таблицы
	 */
	private SelenideElement getDeactivateButton() {
		return findElement(getFirstRow().$$(".row-button-panel button"), TITLE, "Деактивировать");
	}

	/**
	 * Возвращает диалог деактивации записи в таблице
	 *
	 * @return диалог деактивации записи в таблице
	 */
	private SelenideElement getDeactivateDialog() {
		return findElement($$(".ui-confirm-dialog"), "деактивировать");
	}

	/**
	 * Возвращает первую строку в таблице
	 *
	 * @return первая строка в таблице
	 */
	private SelenideElement getFirstRow() {
		return getTableData().$("tbody tr");
	}
}
