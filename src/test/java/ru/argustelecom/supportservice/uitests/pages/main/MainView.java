package ru.argustelecom.supportservice.uitests.pages.main;

import ru.argustelecom.supportservice.uitests.pages.AbstractArgusView;
import ru.argustelecom.supportservice.uitests.pages.dictionary.ProblemCloseCodeDirView;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

/**
 * Главная страница
 *
 * @author n.sofronov
 */
public class MainView extends AbstractArgusView {

	/**
	 * Стандартный конструктор
	 */
	public MainView() {
		this(false);
	}

	/**
	 * Конструктор открывающий главную страницу
	 *
	 * @param ifOpen нужно ли открывать главную страницу
	 */
	public MainView(boolean ifOpen) {
		if (ifOpen) open("");
	}

	/**
	 * Выбирает вкладку в центре страницы
	 *
	 * @param index - номер выбираемой вкладки, нумерация начинается с нуля
	 * @return MainView
	 */
	public MainView selectAboutSystemTab(int index) {
		$("#about_system .ui-tabs-nav li", index).click();
		return this;
	}
}
