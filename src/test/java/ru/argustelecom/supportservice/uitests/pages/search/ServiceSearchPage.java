package ru.argustelecom.supportservice.uitests.pages.search;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

import org.openqa.selenium.By;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import ru.argustelecom.supportservice.uitests.pages.AbstractArgusView;

/**
 * Created by el.vasilyeva on 01.06.2017.
 */
public class ServiceSearchPage extends AbstractArgusView {

    public ElementsCollection getSearchResult(){
        return $$(By.className("search-result-client-link"));
    };

    public ServiceSearchPage setServiceName(String ServiceName) {
        $(By.id("search_params_form-service_name")).setValue(ServiceName);
        return this;
    }

    public ServiceSearchPage setServiceAdress(String ServiceAdress) {
        $(By.id("search_params_form-address-address_installation_input")).setValue(ServiceAdress);
        return this;
    }

    public ServiceSearchPage setOffice (String clientOffice) {
        $(By.id("search_params_form-location")).setValue(clientOffice);
        return this;
    }

    public ServiceSearchPage selectAll() {
        $(By.xpath(".//*[@id='search_params_form-client_type']/div[1]/span")).click();
        return this;
    }

    public ServiceSearchPage selectOrg() {
        $(By.xpath(".//*[@id='search_params_form-client_type']/div[1]/span")).click();
        return this;
    }

    public ServiceSearchPage selectAbon() {
        $(By.xpath(".//*[@id='search_params_form-client_type']/div[1]/span")).click();
        return this;
    }

    public ServiceSearchPage selectName() {
        $(By.xpath(".//*[@id='search_params_form-client_detail_type']/div[1]/span")).click();
        return this;
    }

    public ServiceSearchPage selectNLS() {
        $(By.xpath(".//*[@id='search_params_form-client_detail_type']/div[2]/span")).click();
        return this;
    }

    public ServiceSearchPage selectINN() {
        $(By.xpath(".//*[@id='search_params_form-client_detail_type']/div[3]/span")).click();
        return this;
    }

    public ServiceSearchPage setClientDetail (String clientDetail) {
        $(By.id("search_params_form-client_detail")).setValue(clientDetail);
        return this;
    }

    //public ServiceSearchPage setClientDetail (String clientDetail) {
        //$(By.id("search_params_form-client_detail")).setValue(clientDetail);
        //return this;
    //}



    public ServiceSearchPage setServiceType(String serviceType) {
        $("#search_params_form-service_type_input").setValue(serviceType);
        $$("#search_params_form-service_type_panel li").get(0).click();
        return this;
    }

    public ServiceSearchPage setServiceType(int index) {
        $(By.xpath(".//*[@id='search_params_form-service_type']/button")).click();
        $$("#search_params_form-service_type_panel li").get(index).click();
        return this;
    }

    public String getServiceType() {
        return $("#search_params_form-service_type_input").getValue();
    }


    public ServiceSearchPage setTechnologyType(String serviceType) {
        $("#search_params_form-inst_load_type_input").setValue(serviceType);
        $$("#search_params_form-inst_load_type_panel li").get(0).click();
        return this;
    }

    public ServiceSearchPage setTechnologyType(int index) {
        $(By.xpath(".//*[@id='search_params_form-inst_load_type']/button")).click();
        $$("#search_params_form-inst_load_type_panel li").get(index).click();
        return this;
    }

    public String getTechnologyType() {
        return $("#search_params_form-inst_load_type_input").getValue();
    }

    public ServiceSearchPage clickSearchButton() {
        $("#search_params_form-redirect_search").click();
        return this;
    }

    public ServiceSearchPage clickClean(){
        $(By.id("search_params_form-clean")).click();
        return this;
    }

    //public ServiceSearchPage setAllowDeactivated() {
    //    $(By.id("search_params_form-allow_deactivated")).click();
    //    return this;
    //}

    public ServiceSearchPage selectClientType(String clientType) {
        if ("ВСЕ".equals(clientType.toUpperCase())) {
            $(By.xpath(".//*[@id='search_params_form-client_type']/div[1]/span")).click();
        }
        if ("ЮЛ".equals(clientType.toUpperCase())) {
            $(By.xpath(".//*[@id='search_params_form-client_type']/div[2]/span")).click();
        }
        if ("ФЛ".equals(clientType.toUpperCase())) {
            $(By.xpath(".//*[@id='search_params_form-client_type']/div[3]/span")).click();
        }
        return this;
    }

    //public ElementsCollection getSearchResult() {
    //    return $$(By.className("search-result-client-link"));
    //}

     public ServiceSearchPage setAllowDeactivated(boolean allowDeactivated) {
        if (allowDeactivated) {
            if (!$("#search_params_form-allow_deactivated_input").is(Condition.checked)) {
                $("#search_params_form-allow_deactivated").click();
            }
        } else {
            if ($("#search_params_form-allow_deactivated_input").is(Condition.checked)) {
                $("#search_params_form-allow_deactivated").click();
            }
        }
        return this;
    }

}