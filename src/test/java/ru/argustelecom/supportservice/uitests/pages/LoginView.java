package ru.argustelecom.supportservice.uitests.pages;

import com.codeborne.selenide.Condition;
import ru.argustelecom.supportservice.uitests.PropertiesService;
import ru.argustelecom.supportservice.uitests.pages.main.MainView;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static ru.argustelecom.supportservice.uitests.PropertiesService.SERVER_ADDRESS;
import static ru.argustelecom.supportservice.uitests.PropertiesService.SERVER_LOGIN;
import static ru.argustelecom.supportservice.uitests.PropertiesService.SERVER_PASSWORD;

/**
 * Страница входа на аппсервер, открывается незалогиненному пользхователю
 * 
 * Created by el.vasilyeva on 29.05.2017.
 */
public class LoginView extends AbstractArgusView {

	/**
	 * Выполняет стандартный вход в систему
	 * 
	 * @return MainView открывается после логина
	 */
	public static MainView login(){
		return login(PropertiesService.getProperty(SERVER_LOGIN), PropertiesService.getProperty(SERVER_PASSWORD));
	}

	/**
	 * Выполняет вход в систему под учётной записью
	 * 
	 * @param name логин
	 * @param password пароль
	 * @return MainView открывается после логина
	 */
	public static MainView login(String name, String password){
		LoginView loginView = new LoginView();
		loginView.setLoginName(name);										// ввести логин
		loginView.setPassword(password);									// ввести пароль
		loginView.click();													// нажать на кнопку входа
		$("#wrap_content").waitUntil(Condition.exist, 5000L);	// Дождаться загрузки главной страницы
		return new MainView();
	}

	/**
	 * Открывает страницу логина
	 */
	public LoginView() {
		this(true);
	}

	/**
	 * Создание страницы логина
	 * 
	 * @param ifOpen нужно ли открывать страницу логина
	 */
	public LoginView(boolean ifOpen) {
		if(ifOpen) {
			open("");
		}
	}

	/**
	 * Ввод логина
	 *
	 * @param name - логин
	 * @return LoginView
	 */
	public LoginView setLoginName(String name) {
		$("#login_form-username").setValue(name);
		return this;
	}

	/**
	 * Ввод пароля
	 *
	 * @param password пароль
	 * @return LoginView
	 */
	public LoginView setPassword(String password) {
		$("#login_form-password").setValue(password);
		return this;
	}

	/**
	 * Нажатие на кнопку входа
	 * @return
	 */
	public LoginView click() {
		$("#login_form-submit").click();
		return this;
	}
}
