package ru.argustelecom.supportservice.uitests.pages;

import static com.codeborne.selenide.Selenide.$;
import static ru.argustelecom.supportservice.uitests.DatabaseService.finishConnection;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import com.codeborne.selenide.Condition;
import ru.argustelecom.supportservice.uitests.pages.businessinteraction.GroupProblemView;
import ru.argustelecom.supportservice.uitests.pages.dictionary.ProblemCloseCodeDirView;

/**
 * Стандартная страница Аргус, для наследования других страниц, содержит действия, доступные с любой страницы
 *
 * Created by el.vasilyeva on 02.06.2017.
 */
public class AbstractArgusView {

	/**
	 * Ввод значения в поле autocomplete
     *
	 * @param selector селектор, чтобы найти поле autocomplete
	 * @param index индекс выбираемого значения, нумерация начинается с нуля,
     *              пустая строка не считается, для выбора пустой строки нужно передать -1
	 */
	protected void autocomplite(String selector, int index) {
		selectOneMenu(selector, 1 + index);
	}
	/**
	 * Ввод значения в поле autocomplete
     *
	 * @param selector селектор, чтобы найти поле autocomplete
	 * @param index индекс выбираемого значения, нумерация начинается с нуля
	 */
	protected void selectOneMenu(String selector, int index) {
		SelenideElement autocomplete = $(selector);
		autocomplete.$(".ui-selectonemenu-trigger").click();
		String id = autocomplete.getAttribute("id");
		$("#" + id + "_panel #" + id + "_items .ui-selectonemenu-item", index).click();
	}

	public static LoginView logout() {
		finishConnection();
		$(".argus-ui-icon-logout").click();
		$("#login_panel_container").waitUntil(Condition.exist, 5000L);
		System.out.println("Выполнен выход из системы");
		return new LoginView(false);
	}

	public AbstractArgusView clickServiceSearch() {
		$(By.linkText("Обращения")).hover(); //наводит курсор на "Обращения"
		$(By.linkText("Поиск услуги (ALT+1)")).waitUntil(Condition.visible, 5000).click(); //выбирает "Поиск услуги"
		return this;
	}

	public AbstractArgusView setLoginName(String name) {
		$("#login_form-username").setValue(name);
		return this;
	}

	public AbstractArgusView setPassword(String password) {
		$("#login_form-password").setValue(password);
		return this;
	}

	public AbstractArgusView clickLoginButton() {
		$("#login_form-submit").click();
		return this;
	}

	public AbstractArgusView clickSearviceSearch() {

		$(By.linkText("Обращения")).hover(); //наводит курсор на "Обращения"
		$(By.linkText("Поиск услуги (ALT+1)")).waitUntil(Condition.visible, 5000).click(); //выбирает "Поиск услуги"
		return this;
	}

	public AbstractArgusView clickClientSearch() {

		$(By.linkText("Обращения")).hover(); //наводит курсор на "Обращения"
		$(By.linkText("Поиск клиента (ALT+2)")).waitUntil(Condition.visible, 5000).click(); //выбирает "Поиск клиента"
		return this;
	}

	/**
	 * Нажате на кнопку создания нового группового повреждения
	 *
	 * @return GroupProblemView
	 */
	public GroupProblemView clickNewGroupProblem() {
		SelenideElement menu = $("#mmf-main_menu_bar");
		menu.$(By.linkText("Повреждения")).hover(); //наводит курсор на "Повреждения"
		menu.$(By.linkText("Регистрировать групповое повреждение")).waitUntil(Condition.visible, 5000L).click(); //выбирает "Регистрировать групповое повреждение"
		$("#gi_header-header_frame").waitUntil(Condition.exist, 5000L);
		return new GroupProblemView();
	}

	/**
	 * Откроет страницу справочника ОЧЗ
	 *
	 * @return ProblemCloseCodeDirView
	 */
	public ProblemCloseCodeDirView openProblemCloseCodeDirView() {
		SelenideElement settingsMenu = selectMenuItem(null, "Настройки");
		SelenideElement dictionariesMenu = selectMenuItem(settingsMenu, "Справочники");
		SelenideElement supportServiceMenu = selectMenuItem(dictionariesMenu, "КТП");
		SelenideElement problemCloseCodeMenu = selectMenuItem(supportServiceMenu, "Шифры закрытия");
		problemCloseCodeMenu.click();
		return new ProblemCloseCodeDirView();
	}

	/**
	 * Выбор ссылки в выпадающем меню
	 *
	 * @param parent родительский элемент
	 * @param name имя ссылки
	 * @return выделенный элемент
	 */
	private SelenideElement selectMenuItem(SelenideElement parent, String name) {
		return (parent != null ? parent.$(By.linkText(name)) : $(By.linkText(name))).waitUntil(Condition.visible, 5000).parent().hover();
	}

	public void dialogOkbp(SelenideElement dialog) {
		dialog.$(".ui-dialog-buttonpane button", 0).click();
	}

	public void dialogOkf(SelenideElement dialog) {
		dialog.$(".ui-dialog-footer button", 0).click();
	}

	public void dialogOkb(SelenideElement dialog) {
		dialog.$(".dialog-footer-buttons button", 0).click();
	}

	//	public void fluentWait(final By locator) {
//		$()
//		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
//				.withTimeout(30, TimeUnit.SECONDS)
//				.pollingEvery(5, TimeUnit.SECONDS)
//				.ignoring(NoSuchElementException.class);
//
//		WebElement foo = wait.until(webDriver -> webDriver.findElement(locator));
//	};

	//public void clickSearviceSearch() {
	//}
}
