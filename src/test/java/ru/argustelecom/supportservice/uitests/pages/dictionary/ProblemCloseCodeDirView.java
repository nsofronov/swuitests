package ru.argustelecom.supportservice.uitests.pages.dictionary;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static ru.argustelecom.supportservice.uitests.SWUiTestUtilites.findElement;

/**
 * Страница справочника шифров закрытия
 *
 * @author n.sofronov
 */
public class ProblemCloseCodeDirView extends AbstractDictionaryPage {
	/**
	 * Открываем контекстное меню в дереве категория шифров закрытия
	 *
	 * @param name	- имя категории контекстное меню которой нужно открыть, может быть null, тогда откроем у первой попавшейся
	 * @param menu	- действие, которое нужно выполнить в контекстном меню
	 * @return диалог, полученный после нажатия в контекстном меню дерева
	 */
	public SelenideElement contextMenuClick(String name, ProblemCloseCodeDirViewContextMenu menu) {
		if (name != null) findCodeCategory(getCloseGroupTree(), name).hover().contextClick();	// Если прислали имя, наводимся на нужную категорию и открываем контекстоное меню
		else getCloseGroupTree().$("li .ui-treenode-label").hover().contextClick();				// Если имени не прислали, то наводимся на первую попавшую
		SelenideElement contextMenu = $$(".ui-contextmenu").stream()
				.filter(e -> !e.attr("style").contains("display: none")).findFirst()
				.orElseThrow(() -> new RuntimeException("Не удалось найти контекстное меню"));		// Открываем контекстное меню
		contextMenu.$(By.linkText(menu.getName())).waitUntil(Condition.visible, 5000).click();	// Выбираем действие в контекстном меню
		switch (menu) {
		case ADD: return $("#create_new_code_group_form");
		case REMOVE: return $("#confirm_delete_dialog");
		case EDIT: return $("#edit_code_group");
		default: return null;}
	}

	public void fillCreateCodeCategoryDialog(SelenideElement dialog, String parent, String name) {
		if (parent == null) {
			dialog.$("#create_new_code_group_form-selected_from_input").setValue("");
		} else {
			dialog.$("#create_new_code_group_form-selected_from_input").setValue(parent);
			$("#create_new_code_group_form-selected_from_panel li", 0).waitUntil(Condition.visible, 5000).click();
		}
		dialog.$("#create_new_code_group_form-new_code_group_name").setValue(name);
	}

	public void fillEditCodeCategoryDialog(SelenideElement dialog, String name) {
		dialog.$("#edit_code_group_form-edit_code_group_name").setValue(name);
	}

	public void fillAddDialog(SelenideElement dialog, String codeName, String category, String description) {
		dialog.$("#create_item_form-cc_name").setValue(codeName);
		dialog.$("#create_item_form-cc_group .ui-selectonemenu-trigger").click();
		findElement($$("#create_item_form-cc_group_panel .ui-selectonemenu-item"), category).click();
		dialog.$("#create_item_form-cc_description").setValue(description);
	}

	public SelenideElement findCodeCategory(String parent, String name) {
		if (parent != null) {
			SelenideElement codeCategory = findCodeCategory(getCloseGroupTree(), parent);
			codeCategory.$(".ui-tree-toggler").click();
			codeCategory = codeCategory.parent();
			codeCategory.$(".ui-treenode").waitUntil(Condition.visible, 5000);
			return findCodeCategory(codeCategory, name);
		} else {
			return findCodeCategory(getCloseGroupTree(), name);
		}
	}

	private SelenideElement findCodeCategory(SelenideElement parent, String name) {
		return findElement(parent.$$(".ui-treenode .ui-treenode-content"), name);
	}

	private SelenideElement getCloseGroupTree() {
		return $("#side_form-close_code_group_tree");
	}

	private SelenideElement getTableHead() {
		return $("#edit_form-edit_table_head");
	}

	public void filterByCodeName(String codeName) {
		SelenideElement tableHead = getTableHead();
		findElement(tableHead.$$("tr th"), "Шифр").$("input").setValue(codeName);
		SelenideElement categorySelect = findElement(getTableHead().$$("tr th"), "Категория").$(".ui-selectonemenu");
		categorySelect.$(".ui-selectonemenu-trigger").click();
		$("#" + categorySelect.getAttribute("id") + "_panel .ui-selectonemenu-item").click();
		findElement(getTableData().$$("tbody tr"), codeName);
	}

	public String codeCategoryName(String name) {
		return name.length() > 32 ? name.substring(0, 32) : name;
	}

	public enum ProblemCloseCodeDirViewContextMenu {
		ADD("Добавить"), EDIT("Редактировать"), REMOVE("Удалить");

		ProblemCloseCodeDirViewContextMenu(String name) {
			this.name = name;
		}

		private String name;

		public String getName() {
			return name;
		}
	}
}
