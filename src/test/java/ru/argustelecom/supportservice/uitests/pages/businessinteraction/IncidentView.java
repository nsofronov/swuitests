package ru.argustelecom.supportservice.uitests.pages.businessinteraction;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import ru.argustelecom.supportservice.uitests.pages.AbstractArgusView;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

/**
 * Страница инцидента.
 *
 * @author n.sofronov
 */
public class IncidentView extends AbstractArgusView {

	/**
	 * Стандартный конструктор
	 */
	public IncidentView() {
	}

	/**
	 * Конструктор, который откроет страницу, по переданному id
	 *
	 * @param id - идентификатор
	 */
	public IncidentView(String id) {
		open("views/supportservice/incident/IncidentView.xhtml?businessInteraction=BusinessInteraction-" + id);
		$("#bi_header-header_frame .heading-incident").waitUntil(Condition.exist, 5000L).shouldHave(Condition.text("ИНЦИДЕНТ"));
	}

	/**
	 * Устанавливает тип услуги для инцидента
	 *
	 * @param index номер вводимого типа услуги
	 * @return IncidentView
	 */
	public IncidentView setServiceType(int index) {
		autocomplite("#incident_form-information-service_type", index);
		return this;
	}

	/**
	 * Устанавливает тип инцидента
	 *
	 * @param index номер вводимого типа инцидента
	 * @return IncidentView
	 */
	public IncidentView setIncidentType(int index) {
		autocomplite("#incident_form-information-incident_type", index);
		return this;
	}

	/**
	 * Создаёт кондакт типа email
	 *
	 * @return
	 */
	public IncidentView createEmailContact() {
		String contactInfoTableId = "#incident_form-information-ii_contacts_info_table";
		$(contactInfoTableId + "-add_ii_contacts_menu").click();								// Нажать на кнопку добавления контакта
		$(contactInfoTableId + "-ii_contacts_menu .ui-menuitem-link", 1).click();		// Выбрать email в выпадайке
		SelenideElement dialog = $("#ii_contact_info_add_dialog");							// Откроется диалог
		SelenideElement contactRow = dialog.$("form .ui-grid .ui-grid-row", 1);					// Заполнить строку контакта
		contactRow.$("input").setValue("nsofronov@argustelecom.ru");									// Ввести email
		contactRow.$("button").click();																	// Нажать на кнопку подтвердить
		SelenideElement selenideElement = dialog.$(".dialog-footer-buttons > .ui-button", 1);		// Кнопка "Добавить"
		selenideElement.waitUntil(Condition.enabled, 5000L).click();									// Подождать, пока разблокируется кнопка "Добавить" и нажать на неё
		dialog.waitUntil(Condition.not(Condition.visible), 5000L);									// Дождаться закрытия диалога
		return this;
	}

	/**
	 * Выполнение действия по процессу
	 *
	 * @param index индекс выполяемого действия
	 */
	public void signal(int index){
		autocomplite("#signal_form-available_transitions", index);				// Выбрать решение
		$("#signal_form-compleate").waitUntil(Condition.enabled, 5000L).click();	// Нажать на кнопку завершить
	}

	/**
	 * Закрытие инцидента
	 */
	public void close(){
		signal(1);																						// Выбрать действие "Закрыть"
		SelenideElement dialog = $("#signal_process_dialog").waitUntil(Condition.visible, 5000L);	// Откроется диалог с выбором ШЗ
		autocomplite("#signal_process_dialog_form .ui-selectonemenu", 0);							// Выбрать ШЗ в поле
		dialog.$(".dialog-footer-buttons button", 0).click();												// Нажать на "Сохранить"
		$("#closed_incident_footer_frame .container-simple").waitUntil(Condition.exist, 5000L);	// Дождаться обновления страницы
	}

	/**
	 * Возвращает элемент хедера
	 *
	 * @return элемент хедера
	 */
	public SelenideElement getHeader() {
		return $("#bi_header-header_frame .ui-grid");
	}
}
