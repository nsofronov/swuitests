package ru.argustelecom.supportservice.uitests.pages.businessinteraction;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import ru.argustelecom.supportservice.uitests.pages.AbstractArgusView;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

/**
 * Страница группового повреждения
 *
 * @author n.sofronov
 */
public class GroupProblemView extends AbstractArgusView {

	/**
	 * Простой конструктор
	 */
	public GroupProblemView() {
	}

	/**
	 * Конструктор, выполняющий открытие группового повреждения
	 *
	 * @param id - id группового повреждения
	 */
	public GroupProblemView(String id) {
		open("views/supportservice/groupproblem/GroupProblemView.xhtml?businessInteraction=BusinessInteraction-" + id);
		$("#gi_header-header_frame .heading-group-problem").waitUntil(Condition.exist, 5000L).shouldHave(Condition.text("ГРУППОВОЕ ПОВРЕЖДЕНИЕ"));
	}

	/**
	 * Создание правила по региону
	 *
	 * @param region - регион
	 * @param serviceTypeIndex - тип услуги
	 */
	public void createRegionRule(String region, int serviceTypeIndex) {
		selectRuleType(0);
		SelenideElement regionRule = $("#group_interaction_rule_frame_form-create_rule_panel");
		regionRule.$("#group_interaction_rule_frame_form-region_rule_address-region_rule_address input").setValue(region);
		$("#group_interaction_rule_frame_form-region_rule_address-region_rule_address_panel .ui-autocomplete-item")
				.waitUntil(Condition.exist, 5000L).click();

		SelenideElement autocomplete = $("#group_interaction_rule_frame_form-region_rule_service_type_list");
		autocomplete.$(".ui-autocomplete-dropdown").click();
		String id = autocomplete.getAttribute("id");
		$("#group_interaction_rule_frame_form-region_rule_service_type_list_panel .ui-autocomplete-item", 0).click();

		regionRule.$(".ui-grid-col-6",1).$(".ui-grid-row", 1).$("button").click();
		$("#group_interaction_info_form-tab_view-group_interaction_rule_table_data button").waitUntil(Condition.exist, 5000L);
	}
	/**
	 * Выбор типа правила
	 *
	 * @param index индекс
	 */
	public GroupProblemView selectRuleType(int index) {
		selectOneMenu("#group_interaction_rule_frame_form-selected_group_problem_rule_type", index);
		return this;
	}

	/**
	 * Нажатие на кнопку "Сохранить"
	 */
	public void save() {
		$("#global_editor-form-save").click();
		$("#history_layout_unit").waitUntil(Condition.exist, 5000L);
	}

	/**
	 * Закрытие игруппового повреждения
	 */
	public void close() {
		$("#signal_form-compleate").click();
		SelenideElement dialog = $("#signal_process_dialog");													// Откроется диалог с выбором ШЗ
		autocomplite("#signal_process_dialog_form .ui-selectonemenu", 0);									// Выбрать ШЗ в поле
		dialog.$(".dialog-footer-buttons button", 0).click();														// Нажать на "Сохранить"
		$("#closed_group_interaction_footer_frame").waitUntil(Condition.exist, 5000L);	// Дождаться обновления страницы
	}

	/**
	 * Возвращает элемент хедера
	 *
	 * @return элемент хедера
	 */
	public SelenideElement getHeader() {
		return $("#gi_header-header_frame .ui-grid");
	}
}
