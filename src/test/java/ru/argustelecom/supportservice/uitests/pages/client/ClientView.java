package ru.argustelecom.supportservice.uitests.pages.client;

import ru.argustelecom.supportservice.uitests.pages.AbstractArgusView;
import ru.argustelecom.supportservice.uitests.pages.businessinteraction.IncidentView;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

/**
 * Страница клиента
 *
 * @author n.sofronov
 */
public class ClientView extends AbstractArgusView {
	/**
	 * Создание страницы клиента.
	 * Предварительно эта страница будет открыта.
	 *
	 * @param clientNumber id клиента
	 */
	public ClientView(Long clientNumber) {
		open("/views/registercontact/clientcard/ClientCardView.xhtml?client=Client-" + clientNumber);
	}

	/**
	 * Стандартный конструктор без открытия.
	 */
	public ClientView() {

	}

	/**
	 * Нажатие на кнопку создания инцидента на неизвестную услугу.
	 *
	 * @return - откроется IncidentView
	 */
	public IncidentView clickRegisterIncidentUnknownService() {
		$("#register_incident_form-register_unknown_incident_btn").click();
//		$("#register_incident_dialog .ui-dialog-content .ui-button.ui-priority-primary").click();
		return new IncidentView();
	}
}
