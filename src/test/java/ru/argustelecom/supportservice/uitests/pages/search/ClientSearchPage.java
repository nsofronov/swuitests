package ru.argustelecom.supportservice.uitests.pages.search;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

import org.openqa.selenium.By;

import com.codeborne.selenide.ElementsCollection;
import ru.argustelecom.supportservice.uitests.pages.AbstractArgusView;

/**
 * Created by el.vasilyeva on 01.06.2017.
 */
public class ClientSearchPage extends AbstractArgusView {

    /*public ClientSearchPage setClientDetails(String name) {
        $("#search_params_form-client_detail").setValue(name);
        return this;
    }
    */

//    public ClientSearchPage setClientType(String type) {
//        $("#search_params_form-client_type").selectOption(type);
//        return this;
//    }



    public ClientSearchPage selectAll() {
        $(By.xpath(".//*[@id='search_params_form-client_type']/div[1]/span")).click();
        return this;
    }

    public ClientSearchPage selectOrg() {
        $(By.xpath(".//*[@id='search_params_form-client_type']/div[2]/span")).click();
        return this;
    }

    public ClientSearchPage selectAbon() {
        $(By.xpath(".//*[@id='search_params_form-client_type']/div[3]/span")).click();
        return this;
    }

    public ClientSearchPage selectName() {
        $(By.xpath(".//*[@id='search_params_form-client_detail_type']/div[1]/span")).click();
        return this;
    }

    public ClientSearchPage selectNLS() {
        $(By.xpath(".//*[@id='search_params_form-client_detail_type']/div[2]/span")).click();
        return this;
    }

    public ClientSearchPage selectINN() {
        $(By.xpath(".//*[@id='search_params_form-client_detail_type']/div[3]/span")).click();
        return this;
    }

    public ClientSearchPage setClientDetail (String clientDetail) {
        $(By.id("search_params_form-client_detail")).setValue(clientDetail);
        return this;
    }

    public ClientSearchPage selectContactAndess(){
        $(By.xpath(".//*[@id='search_params_form-j_idt201']/div[1]/span")).click();
        return this;
    }

    public ClientSearchPage selectClientAdress(){
        $(By.xpath(".//*[@id='search_params_form-j_idt201']/div[2]/span")).click();
        return this;
    }

    public ClientSearchPage setAdress (String clientAdress) {
        $(By.id("search_params_form-address-address_client_input")).setValue(clientAdress);
        return this;
    }


    public ClientSearchPage setOffice (String clientOffice) {
        $(By.id("search_params_form-location")).setValue(clientOffice);
        return this;
    }

    public ClientSearchPage setContactPhone(String contactPhone) {
        $("#search_params_form-phone").setValue(contactPhone);
        return this;
    }

    public ClientSearchPage clickSearchButton() {
        $("#search_params_form-redirect_search").click();
        return this;
    }

    public ClientSearchPage clickClean(){
        $(By.id("search_params_form-clean")).click();
        return this;
    }


    public String getClientDetail() {
        return $(By.id("search_params_form-client_detail")).getValue();
    }

    public String getContactPhone() {
        return $(By.id("search_params_form-phone")).getValue();
    }

    public String getAdress() {
        return $(By.id("search_params_form-address-address_client")).getValue();
    }

    public String getOffice() {
        return $(By.id("search_params_form-location")).getValue();
    }


    public String getClientINN() {
        return $(By.xpath(".//*[@id='client_edit_form-j_idt200']")).getText();
    }

    public ClientSearchPage selectClientType(String clientType) {
        if ("ВСЕ".equals(clientType.toUpperCase())) {
            $(By.xpath(".//*[@id='search_params_form-client_type']/div[1]/span")).click();
        }
        if ("ЮЛ".equals(clientType.toUpperCase())) {
            $(By.xpath(".//*[@id='search_params_form-client_type']/div[2]/span")).click();
        }
        if ("ФЛ".equals(clientType.toUpperCase())) {
            $(By.xpath(".//*[@id='search_params_form-client_type']/div[3]/span")).click();
        }
        return this;
    }


    public ElementsCollection getSearchResult() {
        return $$(By.className("search-result-client-link"));
    }


    /*public ClientSearchPage setPassword(String password) {
        $("#login_form-password").setValue(password);
        return this;
    }

    public ClientSearchPage click() {
        $("#login_form-submit").click();
        return this;
    }
*/
}
