package ru.argustelecom.supportservice.uitests.cases.businessinteraction;

import ru.argustelecom.supportservice.uitests.pages.businessinteraction.GroupProblemView;
import ru.argustelecom.supportservice.uitests.pages.main.MainView;

/**
 * Набор бизнесс действий для работы с групповыми повреждениями
 * 
 * @author n.sofronov
 */
public class GroupProblemCase {

	/**
	 * Регистрация нового группового повреждения
	 */
	public static GroupProblemView registerGroupProblem() {
		GroupProblemView groupProblemView = new MainView().clickNewGroupProblem();
		groupProblemView.createRegionRule("г. Санкт-Петербург", 0);
		groupProblemView.save();
		return groupProblemView;
	}



	public static void closeGroupProblem(GroupProblemView groupProblemView) {
		groupProblemView.close();
	}
}
