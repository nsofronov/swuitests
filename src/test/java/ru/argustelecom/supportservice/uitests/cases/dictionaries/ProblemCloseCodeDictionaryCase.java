package ru.argustelecom.supportservice.uitests.cases.dictionaries;

import com.codeborne.selenide.SelenideElement;
import ru.argustelecom.supportservice.uitests.pages.dictionary.ProblemCloseCodeDirView;

import static ru.argustelecom.supportservice.uitests.pages.dictionary.ProblemCloseCodeDirView.ProblemCloseCodeDirViewContextMenu.ADD;
import static ru.argustelecom.supportservice.uitests.pages.dictionary.ProblemCloseCodeDirView.ProblemCloseCodeDirViewContextMenu.EDIT;
import static ru.argustelecom.supportservice.uitests.pages.dictionary.ProblemCloseCodeDirView.ProblemCloseCodeDirViewContextMenu.REMOVE;

/**
 * Набор действий для работы со справочником шифров закрытия
 * 
 * @author n.sofronov
 */
public class ProblemCloseCodeDictionaryCase {
	/**
	 * Создание категории ШЗ
	 * 
	 * @param view		- указатель на вью в котором будем производить изменения
	 * @param parent	- родительская категория, может быть null, тогда новая категория создастся в корне дерева
	 * @param name		- предполагаемое имя новой категории
	 * @return имя новой категории
	 */
	public static String createCodeCategory(ProblemCloseCodeDirView view, String parent, String name) {
		name = view.codeCategoryName(name);									// Обрезаем имя в соответствии с требованиями модели
		SelenideElement dialog = view.contextMenuClick(null, ADD);	// Откываем контекстное меню дерева ШЗ
		view.fillCreateCodeCategoryDialog(dialog, parent, name);			// Заполняем поля в диалоге создания категории ШЗ
		view.dialogOkb(dialog);												// Подтверждаем изменения
		System.out.println("Создана категория " + name + " в " + parent);	// Логируем успешность создания
		return name;
	}

	/**
	 * Редактирование категории ШЗ
	 * 
	 * @param view		- указатель на вью в котором будем производить изменения
	 * @param name		- имя категории, котороую нужно отредактировать
	 * @param newName	- предполагаемое новое имя категории
	 * @return новое имя категории
	 */
	public static String editCodeCategory(ProblemCloseCodeDirView view, String name, String newName) {
		newName = view.codeCategoryName(name);						// Обрезаем имя в соответствии с требованиями модели
		SelenideElement dialog = view.contextMenuClick(name, EDIT);	// Откываем контекстное меню дерева ШЗ
		view.fillEditCodeCategoryDialog(dialog, newName);			// Заполняем поля в диалоге редавтирования категории ШЗ
		view.dialogOkb(dialog);										// Подтверждаем изменения
		System.out.println("Отредактирована категория " + newName);	// Логируем успешность редавтирования
		return name;
	}

	/**
	 * Удаление категории шифров закрытия
	 *
	 * @param view	- указатель на вью в котором будем производить изменения
	 * @param name	- имя категории, котороую нужно удалить
	 */
	public static void removeCodeCategory(ProblemCloseCodeDirView view, String name) {
		SelenideElement dialog = view.contextMenuClick(name, REMOVE);	// Откываем контекстное меню дерева ШЗ
		view.dialogOkf(dialog);											// Подтверждаем удаление
		System.out.println("Удалена категория " + name);				// Логируем успешность удаления
	}

	/**
	 * Создание шифра закрытия
	 *
	 * @param view			- указатель на вью в котором будем производить изменения
	 * @param codeName		- новое название шифра закрытия
	 * @param category		- категория, в которой будет располагаться новый шифр
	 * @param description	- описание шифра закрытия
	 * @return новое название шифра закрытия
	 */
	public static String createCloseCode(ProblemCloseCodeDirView view, String codeName, String category, String description) {
		SelenideElement dialog = view.clickAddButton();					// Нажимаем на кнопку создания новой записи
		view.fillAddDialog(dialog, codeName, category, description);	// Заполняем поля в диалоге создания ШЗ
		view.dialogOkb(dialog);											// Подтверждаем изменения
		System.out.println("Создан шифр закрытия " + codeName);			// Логируем успешность выполнения создания
		return codeName;
	}

	/**
	 * Редактирование шифра закрытия
	 *
	 * @param view			- указатель на вью в котором будем производить изменения
	 * @param codeName		- имя изменяемого шифра закрытия
	 * @param newCodeName	- новое название шифра закрытия
	 * @return новое название шифра закрытия
	 */
	public static String editCloseCode(ProblemCloseCodeDirView view, String codeName, String newCodeName) {
		view.filterByCodeName(codeName);										// Отфильтруем так, чтобы мы видели только изменяемый шифр закрытия
		view.clickEditRow();													// Включим редактирование строки
		view.editRowField(codeName, newCodeName);								// Внесём изменения
		view.saveEdit();														// Подтверждаем изменения
		System.out.println("Шифр закрытия " + newCodeName + " был изменён");	// Логируем успешность выполнения изменения
		return newCodeName;
	}

	/**
	 * Деактивация шифра закрытия
	 *
	 * @param view		- указатель на вью в котором будем производить изменения
	 * @param codeName	- имя деактивируемого шифра закрытия
	 */
	public static void deactivateCode(ProblemCloseCodeDirView view, String codeName) {
		view.filterByCodeName(codeName);										// Отфильтруем так, чтобы мы видели только изменяемый шифр закрытия
		view.clickDeactivate();													// Нажимаем кнопку деактивации
		System.out.println("Шифр закрытия " + codeName + " был деактивирован");	// Логируем успешность выполнения деактивации
	}
}
