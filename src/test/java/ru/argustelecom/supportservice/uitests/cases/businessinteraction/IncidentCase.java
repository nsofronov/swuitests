package ru.argustelecom.supportservice.uitests.cases.businessinteraction;

import ru.argustelecom.supportservice.uitests.pages.businessinteraction.IncidentView;
import ru.argustelecom.supportservice.uitests.pages.client.ClientView;

import java.io.Serializable;

/**
 * Набор стандартных бизнесс действий для инцидентов
 *
 * @author n.sofronov
 */
public class IncidentCase implements Serializable {

	/**
	 * Создание инцидента на неизвестную услугу
	 *
	 * @param clientId id клиента
	 * @return IncidentView
	 */
	public static IncidentView registerIncidentUnknownService(Long clientId){
		ClientView clientPage = new ClientView(clientId);
		IncidentView incidentView = clientPage.clickRegisterIncidentUnknownService();
		incidentView.setServiceType(0);
		incidentView.setIncidentType(0);
		incidentView.createEmailContact();
		incidentView.signal(1);
		return incidentView;
	}

	/**
	 * Закрытие инцидента
	 *
	 * @param incidentView
	 */
	public static void closeIncident(IncidentView incidentView) {
		incidentView.close();
	}
}
