package ru.argustelecom.supportservice.uitests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import java.util.UUID;
import java.util.function.Function;

import static ru.argustelecom.supportservice.uitests.PropertiesService.SERVER_ADDRESS;

/**
 * Класс, для различных утилит для работы UI тестов
 *
 * @author nsofronov
 */
public class SWUiTestUtilites {

	/**
	 * Выполнение подготовки для запуска UI тестов
	 */
	public static void prepare() {
		prepare(PropertiesService.getProperty(SERVER_ADDRESS));
	};

	/**
	 * Выполнение подготовки для запуска UI тестов
	 *
	 * @param url - адресс сервера
	 */
	public static void prepare(String url) {
		System.setProperty("webdriver.chrome.driver", ".\\src\\test\\resources\\driver\\chromedriver_v2.29.exe");
		Configuration.baseUrl = url;
		Configuration.browser = "chrome";
	}

	/**
	 * Придумывает рандомное имя
	 *
	 * @param prefix префикс имени
	 * @return новое имя
	 */
	public static String randomName(String prefix) {
		return prefix + " - " + UUID.randomUUID().toString();
	}

	/**
	 * Находит элемент с определённым текстом
	 *
	 * @param selenideElements
	 * @param text
	 * @return
	 */
	public static SelenideElement findElement(ElementsCollection selenideElements, String text) {
		return findElement(selenideElements, SelenideElement::text, text);
	}

	/**
	 * Находит элемент с определённым текстом
	 *
	 * @param selenideElements
	 * @param function
	 * @param text
	 * @return
	 */
	public static SelenideElement findElement(ElementsCollection selenideElements, Function<SelenideElement, String> function,
			String text) {
		return selenideElements.stream().filter(b -> function.apply(b).contains(text))
				.findFirst().orElseThrow(() -> new RuntimeException("Не нашли элемент с текстом " + text));
	}
}
